<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome', [
    //     'name' => 'Willian'
    // ]);

    // return view('welcome')->with('name', 'Willian Ferreira');
    
    $tasks = ['Estudar Laravel', 'Tomar um café', 'Fazer um Freela'];

    return view('welcome', compact('tasks'));
});

Route::get('about', function () {
    
    return view('about');
});
